package com.nnikitafrolov.pmsystem;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Class that contains method for read database.properties
 *
 * @author Nikita Frolov
 */
public class FileHelper {
    public static String getPropValues(String nameProperty) throws IOException {
        Properties prop = new Properties();
        String propFileName = "database.properties";
        try(InputStream inputStream = FileHelper.class.getClassLoader().getResourceAsStream(propFileName)) {
            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
            }
            return prop.getProperty(nameProperty);
        }
    }
}
