package com.nnikitafrolov.pmsystem;

import com.nnikitafrolov.pmsystem.dao.PersistException;
import com.nnikitafrolov.pmsystem.dao.hibernate.HibernateDAOFactory;
import com.nnikitafrolov.pmsystem.dao.jdbс.MySqlDaoFactory;
import com.nnikitafrolov.pmsystem.view.ConsoleHelper;
import org.hibernate.SessionFactory;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

public class PMSystemRunner {

    public static void main(String[] args) throws PersistException, IOException, SQLException {
        ConsoleHelper<SessionFactory> consoleHelper = new ConsoleHelper<>(new HibernateDAOFactory());
        consoleHelper.consoleHelp();
    }
}
