package com.nnikitafrolov.pmsystem.controller;

import com.nnikitafrolov.pmsystem.dao.GenericDao;
import com.nnikitafrolov.pmsystem.model.Identified;
import com.nnikitafrolov.pmsystem.dao.PersistException;

import java.util.List;

/**
 * An abstract class that extends {@link Controller}
 *
 * @param <T>  type of Controller
 * @param <M>  type of object that controller handles
 * @param <PK> primary key
 * @author Nikita Frolov
 */
public abstract class AbstractController<T extends GenericDao<M, PK>, M extends Identified<PK>, PK extends Long> implements Controller<M, PK> {

    private T entityDAO;

    AbstractController(T entityDAO) {
        this.entityDAO = entityDAO;
    }

    @Override
    public M getById(PK key) throws PersistException {
        return entityDAO.getByPK(key);
    }

    @Override
    public void save(M entity) throws PersistException {
        entityDAO.persist(entity);
    }

    @Override
    public void update(M entity) throws PersistException {
        entityDAO.update(entity);
    }

    @Override
    public void remove(M entity) throws PersistException {
        entityDAO.delete(entity);
    }

    @Override
    public List<M> getAll() throws PersistException {
        return entityDAO.getAll();
    }
}
