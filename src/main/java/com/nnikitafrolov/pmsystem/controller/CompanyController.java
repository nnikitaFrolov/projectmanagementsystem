package com.nnikitafrolov.pmsystem.controller;

import com.nnikitafrolov.pmsystem.dao.GenericDao;
import com.nnikitafrolov.pmsystem.dao.jdbс.MySqlCompanyDao;
import com.nnikitafrolov.pmsystem.model.Company;

import java.sql.Connection;

/**
 * Controller that handles request connected with {@link Company}
 *
 * @author Nikita Frolov
 */
public class CompanyController extends AbstractController<GenericDao<Company, Long>, Company, Long> {
    public CompanyController(GenericDao<Company, Long> companyDao) {
        super(companyDao);
    }
}
