package com.nnikitafrolov.pmsystem.controller;

import com.nnikitafrolov.pmsystem.dao.GenericDao;
import com.nnikitafrolov.pmsystem.model.Customer;

/**
 * Controller that handles request connected with {@link Customer}
 *
 * @author Nikita Frolov
 */
public class CustomerController extends AbstractController<GenericDao<Customer, Long>, Customer, Long> {
    public CustomerController(GenericDao<Customer, Long> customerDao) {
        super(customerDao);
    }
}
