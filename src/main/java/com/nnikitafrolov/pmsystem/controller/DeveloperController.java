package com.nnikitafrolov.pmsystem.controller;

import com.nnikitafrolov.pmsystem.dao.GenericDao;
import com.nnikitafrolov.pmsystem.dao.jdbс.MySqlDeveloperDao;
import com.nnikitafrolov.pmsystem.model.Developer;

import java.sql.Connection;

/**
 * Controller that handles request connected with {@link Developer}
 *
 * @author Nikita Frolov
 */
public class DeveloperController extends AbstractController<GenericDao<Developer, Long>, Developer, Long> {
    public DeveloperController(GenericDao<Developer, Long> developerDAO) {
        super(developerDAO);
    }
}
