package com.nnikitafrolov.pmsystem.controller;

import com.nnikitafrolov.pmsystem.dao.GenericDao;
import com.nnikitafrolov.pmsystem.model.Project;

/**
 * Controller that handles request connected with {@link Project}
 *
 * @author Nikita Frolov
 */
public class ProjectController extends AbstractController<GenericDao<Project, Long>, Project, Long>{
    public ProjectController(GenericDao<Project, Long> projectDAO) {
        super(projectDAO);
    }
}