package com.nnikitafrolov.pmsystem.controller;

import com.nnikitafrolov.pmsystem.dao.GenericDao;
import com.nnikitafrolov.pmsystem.model.Skill;

/**
 * Controller that handles request connected with {@link Skill}
 *
 * @author Nikita Frolov
 */
public class SkillController extends AbstractController<GenericDao<Skill, Long>, Skill, Long>{

    public SkillController(GenericDao<Skill, Long> skillDAO) {
        super(skillDAO);
    }
}
