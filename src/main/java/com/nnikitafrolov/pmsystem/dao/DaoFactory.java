package com.nnikitafrolov.pmsystem.dao;

import com.nnikitafrolov.pmsystem.model.Identified;

import java.io.Serializable;

/**
 * Factory objects for working with the database
 *
 * @param <Context> entity that describes a session with the storage system
 * @author Nikita Frolov
 */
public interface DaoFactory<Context> {

    /**
     * Interface delegate constructor objects
     *
     * @param <T>       entity extends {@link GenericDao}
     * @param <Context>
     */
    interface DaoCreator<T extends GenericDao, Context> {
        /**
         * @param context entity that describes a session with the storage system
         * @return DAO Factory corresponding object 'T'
         */
        T create(Context context);
    }

    /**
     * @return entity that describes a session with the storage system
     */
    Context getContext() throws PersistException;

    /**
     * Exit a session with the storage system
     */
    void closeContext() throws PersistException;

    /**
     * @param context  entity that describes a session with the storage system
     * @param dtoClass class corresponding object type of 'M'
     * @param <M>      object that will corresponding DAO Factory
     * @param <K>      primary key
     * @return returns an object to control the persistent state of an object
     */
    <M extends Identified<K>, K extends Serializable> GenericDao<M, K> getDao(Context context, Class<M> dtoClass) throws PersistException;
}
