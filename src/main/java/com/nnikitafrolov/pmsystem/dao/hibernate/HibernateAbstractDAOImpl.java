package com.nnikitafrolov.pmsystem.dao.hibernate;

import com.nnikitafrolov.pmsystem.dao.GenericDao;
import com.nnikitafrolov.pmsystem.model.Identified;
import com.nnikitafrolov.pmsystem.dao.PersistException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

/**
 * An abstract class that provides a basic implementation of CRUD operations using Hibernate.
 *
 * @author Nikita Frolov
 */
public abstract class HibernateAbstractDAOImpl<T extends Identified<PK>, PK extends Long> implements GenericDao<T, PK> {
    private SessionFactory sessionFactory;

    /**
     * @return class's entity
     */
    public abstract Class<T> getEntityClass();

    /**
     * FROM [Table]
     *
     * @return the hql query to retrieve all records
     */
    public abstract String getSelectQuery();

    HibernateAbstractDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public T persist(T object) throws PersistException {
        Session session = sessionFactory.openSession();
        Transaction transaction;

        transaction = session.beginTransaction();
        session.save(object);
        transaction.commit();
        session.close();
        return getByPK(object.getId());
    }

    @Override
    public T getByPK(PK key) throws PersistException {
        Session session = sessionFactory.openSession();
        Transaction transaction;

        transaction = session.beginTransaction();
        T entity = session.get(getEntityClass(), key);
        if (entity != null) {
            initialization(entity);
        }
        transaction.commit();
        session.close();
        return entity;
    }

    @Override
    public void update(T object) throws PersistException {
        Session session = sessionFactory.openSession();
        Transaction transaction;

        transaction = session.beginTransaction();
        session.merge(object);
        transaction.commit();
        session.close();
    }

    @Override
    public List<T> getAll() throws PersistException {
        Session session = sessionFactory.openSession();
        Transaction transaction;

        transaction = session.beginTransaction();
        List<T> entities = session.createQuery(getSelectQuery(), getEntityClass()).list();
        transaction.commit();
        session.close();
        return entities;
    }

    @Override
    public void delete(T object) throws PersistException {
        Session session = sessionFactory.openSession();
        Transaction transaction;

        transaction = session.beginTransaction();
        session.remove(object);
        transaction.commit();
        session.close();
    }

    /**
     * Force initialization of a proxy or persistent collection.
     *
     * @param object which can initialization
     */
    public abstract void initialization(T object);
}
