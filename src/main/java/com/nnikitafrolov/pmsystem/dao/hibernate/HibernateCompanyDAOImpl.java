package com.nnikitafrolov.pmsystem.dao.hibernate;

import com.nnikitafrolov.pmsystem.model.Company;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;

/**
 * Class that provides a implementation of CRUD  operations for entity {@link Company},
 * that extends {@link HibernateAbstractDAOImpl}
 *
 * @author Frolov Nikita
 */
public class HibernateCompanyDAOImpl extends HibernateAbstractDAOImpl<Company, Long>{
    HibernateCompanyDAOImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public void initialization(Company object) {
        Hibernate.initialize(object.getProjects());
    }


    @Override
    public Class<Company> getEntityClass() {
        return Company.class;
    }

    @Override
    public String getSelectQuery() {
        return "FROM Company";
    }
}
