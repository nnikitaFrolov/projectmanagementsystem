package com.nnikitafrolov.pmsystem.dao.hibernate;

import com.nnikitafrolov.pmsystem.model.Customer;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;

/**
 * Class that provides a implementation of CRUD  operations for entity {@link Customer},
 * that extends {@link HibernateAbstractDAOImpl}
 *
 * @author Frolov Nikita
 */
public class HibernateCustomerDAOImpl extends HibernateAbstractDAOImpl<Customer, Long>{
    HibernateCustomerDAOImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public void initialization(Customer object) {
        Hibernate.initialize(object.getProjects());
    }

    @Override
    public Class<Customer> getEntityClass() {
        return Customer.class;
    }

    @Override
    public String getSelectQuery() {
        return "FROM Customer";
    }
}
