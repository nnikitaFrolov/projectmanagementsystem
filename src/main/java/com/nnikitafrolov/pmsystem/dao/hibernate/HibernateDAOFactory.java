package com.nnikitafrolov.pmsystem.dao.hibernate;

import com.nnikitafrolov.pmsystem.dao.DaoFactory;
import com.nnikitafrolov.pmsystem.dao.GenericDao;
import com.nnikitafrolov.pmsystem.model.*;
import com.nnikitafrolov.pmsystem.dao.PersistException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Factory objects for working with the database using Hibernate that extends {@link DaoFactory}
 *
 * @author Nikita Frolov
 */
public class HibernateDAOFactory implements DaoFactory<SessionFactory> {
    private SessionFactory sessionFactory;

    private Map<Class<? extends Identified>, DaoCreator<? extends GenericDao, SessionFactory>> creators;

    public HibernateDAOFactory() {
        creators = new HashMap<>();
        creators.put(Developer.class, new DaoCreator<HibernateDeveloperDAOImpl, SessionFactory>() {
            @Override
            public HibernateDeveloperDAOImpl create(SessionFactory sessionFactory) {
                return new HibernateDeveloperDAOImpl(sessionFactory);
            }
        });
        creators.put(Skill.class, new DaoCreator<HibernateSkillDAOImpl, SessionFactory>() {
            @Override
            public HibernateSkillDAOImpl create(SessionFactory sessionFactory) {
                return new HibernateSkillDAOImpl(sessionFactory);
            }
        });
        creators.put(Project.class, new DaoCreator<HibernateProjectDAOImpl, SessionFactory>() {
            @Override
            public HibernateProjectDAOImpl create(SessionFactory sessionFactory) {
                return new HibernateProjectDAOImpl(sessionFactory);
            }
        });
        creators.put(Company.class, new DaoCreator<HibernateCompanyDAOImpl, SessionFactory>() {
            @Override
            public HibernateCompanyDAOImpl create(SessionFactory sessionFactory) {
                return new HibernateCompanyDAOImpl(sessionFactory);
            }
        });
        creators.put(Customer.class, new DaoCreator<HibernateCustomerDAOImpl, SessionFactory>() {
            @Override
            public HibernateCustomerDAOImpl create(SessionFactory sessionFactory) {
                return new HibernateCustomerDAOImpl(sessionFactory);
            }
        });
    }

    @Override
    public SessionFactory getContext() throws PersistException {
        sessionFactory = new Configuration().configure("hibernate/hibernate.cfg.xml").buildSessionFactory();
        return sessionFactory;
    }

    @Override
    public void closeContext() throws PersistException {
        sessionFactory.close();
    }

    @Override
    public <M extends Identified<K>, K extends Serializable> GenericDao<M, K> getDao(
            SessionFactory sessionFactory, Class<M> dtoClass) throws PersistException {
        DaoCreator<? extends GenericDao, SessionFactory> creator = creators.get(dtoClass);
        if (creator == null) {
            throw new PersistException("Dao object for " + dtoClass + " not found.");
        }
        return creator.create(sessionFactory);
    }
}
