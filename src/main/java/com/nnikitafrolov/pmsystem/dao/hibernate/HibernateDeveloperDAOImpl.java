package com.nnikitafrolov.pmsystem.dao.hibernate;

import com.nnikitafrolov.pmsystem.model.Developer;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;

/**
 * Class that provides a implementation of CRUD  operations for entity {@link Developer},
 * that extends {@link HibernateAbstractDAOImpl}
 *
 * @author Frolov Nikita
 */
public class HibernateDeveloperDAOImpl extends HibernateAbstractDAOImpl<Developer, Long> {
    HibernateDeveloperDAOImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public void initialization(Developer object) {
        Hibernate.initialize(object.getSkills());
    }

    @Override
    public Class<Developer> getEntityClass() {
        return Developer.class;
    }

    @Override
    public String getSelectQuery() {
        return "FROM Developer";
    }
}
