package com.nnikitafrolov.pmsystem.dao.hibernate;

import com.nnikitafrolov.pmsystem.model.Project;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;

/**
 * Class that provides a implementation of CRUD  operations for entity {@link Project},
 * that extends {@link HibernateAbstractDAOImpl}
 *
 * @author Frolov Nikita
 */
public class HibernateProjectDAOImpl extends HibernateAbstractDAOImpl<Project, Long>{
    @Override
    public Class<Project> getEntityClass() {
        return Project.class;
    }

    @Override
    public String getSelectQuery() {
        return "FROM Project";
    }

    HibernateProjectDAOImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public void initialization(Project object) {
        Hibernate.initialize(object.getDevelopers());
    }
}
