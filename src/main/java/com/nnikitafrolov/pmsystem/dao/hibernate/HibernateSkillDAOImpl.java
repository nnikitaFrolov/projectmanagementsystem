package com.nnikitafrolov.pmsystem.dao.hibernate;

import com.nnikitafrolov.pmsystem.model.Skill;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;

/**
 * Class that provides a implementation of CRUD  operations for entity {@link Skill},
 * that extends {@link HibernateAbstractDAOImpl}
 *
 * @author Nikita Frolov
 */
public class HibernateSkillDAOImpl extends HibernateAbstractDAOImpl<Skill, Long> {

    HibernateSkillDAOImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public void initialization(Skill object) {
        //do nothing
    }

    @Override
    public Class<Skill> getEntityClass() {
        return Skill.class;
    }

    @Override
    public String getSelectQuery() {
        return "FROM Skill";
    }
}
