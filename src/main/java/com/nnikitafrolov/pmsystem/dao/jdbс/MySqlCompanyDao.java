package com.nnikitafrolov.pmsystem.dao.jdbс;

import com.nnikitafrolov.pmsystem.dao.PersistException;
import com.nnikitafrolov.pmsystem.model.Company;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Class that provides a implementation of CRUD  operations for entity {@link Company},
 * that extends {@link AbstractJDBCDao}
 *
 * @author Frolov Nikita
 */
public class MySqlCompanyDao extends AbstractJDBCDao<Company, Long>{

    private static final String SELECT = "SELECT id, name FROM companies";
    private static final String INSERT = "INSERT INTO companies (id, name) VALUES (?, ?);";
    private static final String UPDATE = "UPDATE companies SET name = ? WHERE id= ?;";
    private static final String DELETE = "DELETE FROM companies WHERE id= ?;";

    public MySqlCompanyDao(Connection connection) {
        super(connection);
    }

    @Override
    public String getSelectQuery() {
        return SELECT;
    }

    @Override
    public String getCreateQuery() {
        return INSERT;
    }

    @Override
    public String getUpdateQuery() {
        return UPDATE;
    }

    @Override
    public String getDeleteQuery() {
        return DELETE;
    }

    @Override
    public List<Company> parseResultSet(ResultSet rs) throws PersistException {
        List<Company> result = new LinkedList<>();
        try {
            while (rs.next()) {
                Company company = new Company();
                company.setId(rs.getLong("id"));
                company.setName(rs.getString("name"));
                result.add(company);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public void prepareStatementForInsert(PreparedStatement statement, Company object) throws PersistException {
        try {
            statement.setLong(1, object.getId());
            statement.setString(2, object.getName());
        } catch (Exception e) {
            throw new PersistException(e);
        }
    }

    @Override
    public void prepareStatementForUpdate(PreparedStatement statement, Company object) throws PersistException {
        try {
            statement.setString(1, object.getName());
            statement.setLong(2, object.getId());
        } catch (Exception e) {
            throw new PersistException(e);
        }
    }
}
