package com.nnikitafrolov.pmsystem.dao.jdbс;

import com.nnikitafrolov.pmsystem.dao.PersistException;
import com.nnikitafrolov.pmsystem.model.Customer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Class that provides a implementation of CRUD  operations for entity {@link Customer},
 * that extends {@link AbstractJDBCDao}
 *
 * @author Frolov Nikita
 */
public class MySqlCustomerDao extends AbstractJDBCDao<Customer, Long>{

    private static final String SELECT = "SELECT id, name FROM customers";
    private static final String INSERT = "INSERT INTO customers (id, name) VALUES (?, ?);";
    private static final String UPDATE = "UPDATE customers SET name = ? WHERE id= ?;";
    private static final String DELETE = "DELETE FROM customers WHERE id= ?;";

    public MySqlCustomerDao(Connection connection) {
        super(connection);
    }

    @Override
    public String getSelectQuery() {
        return SELECT;
    }

    @Override
    public String getCreateQuery() {
        return INSERT;
    }

    @Override
    public String getUpdateQuery() {
        return UPDATE;
    }

    @Override
    public String getDeleteQuery() {
        return DELETE;
    }

    @Override
    public List<Customer> parseResultSet(ResultSet rs) throws PersistException {
        List<Customer> customers = new LinkedList<>();
        try{
            while (rs.next()) {
                Customer customer = new Customer();
                customer.setId(rs.getLong("id"));
                customer.setName(rs.getString("name"));
                customers.add(customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

    @Override
    public void prepareStatementForInsert(PreparedStatement statement, Customer object) throws PersistException {
        try{
            statement.setLong(1, object.getId());
            statement.setString(2, object.getName());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void prepareStatementForUpdate(PreparedStatement statement, Customer object) throws PersistException {
        try{
            statement.setString(1, object.getName());
            statement.setLong(2, object.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
