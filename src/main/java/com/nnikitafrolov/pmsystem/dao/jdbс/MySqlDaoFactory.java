package com.nnikitafrolov.pmsystem.dao.jdbс;

import com.nnikitafrolov.pmsystem.FileHelper;
import com.nnikitafrolov.pmsystem.dao.*;
import com.nnikitafrolov.pmsystem.model.*;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Factory objects for working with the database MySql that extends {@link DaoFactory}
 *
 * @author Nikita Frolov
 */
public class MySqlDaoFactory implements DaoFactory<Connection> {

    private Connection connection;

    private String DATABASE_URL;
    private String USER;
    private String PASSWORD;

    private Map<Class<? extends Identified>, DaoCreator<? extends GenericDao, Connection>> creators;

    public MySqlDaoFactory() throws IOException {
        DATABASE_URL = FileHelper.getPropValues("host");
        USER = FileHelper.getPropValues("user");
        PASSWORD = FileHelper.getPropValues("password");

        creators = new HashMap<>();
        creators.put(Developer.class, new DaoCreator<MySqlDeveloperDao, Connection>() {
            @Override
            public MySqlDeveloperDao create(Connection connection) {
                return new MySqlDeveloperDao(connection);
            }
        });
        creators.put(Skill.class, new DaoCreator<MySqlSkillDao, Connection>() {
            @Override
            public MySqlSkillDao create(Connection connection) {
                return new MySqlSkillDao(connection);
            }
        });
        creators.put(Project.class, new DaoCreator<MySqlProjectDao, Connection>() {
            @Override
            public MySqlProjectDao create(Connection connection) {
                return new MySqlProjectDao(connection);
            }
        });
        creators.put(Company.class, new DaoCreator<MySqlCompanyDao, Connection>() {
            @Override
            public MySqlCompanyDao create(Connection connection) {
                return new MySqlCompanyDao(connection);
            }
        });
        creators.put(Customer.class, new DaoCreator<GenericDao, Connection>() {
            @Override
            public MySqlCustomerDao create(Connection connection) {
                return new MySqlCustomerDao(connection);
            }
        });
    }

    @Override
    public Connection getContext() throws PersistException {
        try {
            connection = DriverManager.getConnection(DATABASE_URL, USER, PASSWORD);
        } catch (SQLException e) {
            throw new PersistException(e);
        }
        return  connection;
    }

    @Override
    public void closeContext() throws PersistException {
        try {
            connection.close();
        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    @Override
    public <M extends Identified<K>,K extends Serializable>GenericDao<M,K> getDao(Connection connection, Class<M> dtoClass) throws PersistException {
        DaoCreator<? extends GenericDao, Connection> creator = creators.get(dtoClass);
        if (creator == null) {
            throw new PersistException("Dao object for " + dtoClass + " not found.");
        }
        return creator.create(connection);
    }
}
