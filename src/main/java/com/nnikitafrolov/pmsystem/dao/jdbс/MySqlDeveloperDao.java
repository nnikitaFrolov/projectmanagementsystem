package com.nnikitafrolov.pmsystem.dao.jdbс;

import com.nnikitafrolov.pmsystem.dao.PersistException;
import com.nnikitafrolov.pmsystem.model.Developer;
import com.nnikitafrolov.pmsystem.model.Skill;

import java.sql.*;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Class that provides a implementation of CRUD  operations for entity {@link Developer},
 * that extends {@link AbstractJDBCDao}
 *
 * @author Frolov Nikita
 */
public class MySqlDeveloperDao extends AbstractJDBCDao<Developer, Long> {

    private static final String SELECT = "SELECT id, name FROM developers";
    private static final String INSERT = "INSERT INTO developers (id, name) VALUES (?, ?);";
    private static final String UPDATE = "UPDATE developers SET name = ? WHERE id= ?;";
    private static final String DELETE = "DELETE FROM developers WHERE id= ?;";
    private static final String SELECT_SKILL_DEV = "SELECT id, name FROM skills INNER JOIN (SELECT skill_id FROM skills_developers WHERE developer_id = ?) AS s ON id = s.skill_id;";
    private static final String INSERT_SKILL_DEV = "INSERT INTO skills_developers (skill_id, developer_id) VALUES (?, ?);";
    private static final String DELETE_SKILL_DEV = "DELETE FROM skills_developers WHERE developer_id= ?;";

    MySqlDeveloperDao(Connection connection) {
        super(connection);
    }

    @Override
    public String getSelectQuery() {
        return SELECT;
    }

    @Override
    public String getCreateQuery() {
        return INSERT;
    }

    @Override
    public String getUpdateQuery() {
        return UPDATE;
    }

    @Override
    public String getDeleteQuery() {
        return DELETE;
    }

    @Override
    public List<Developer> parseResultSet(ResultSet rs) throws PersistException {
        LinkedList<Developer> result = new LinkedList<>();
        try {
            while (rs.next()) {
                Developer developer = new Developer();
                developer.setId(rs.getLong("id"));
                developer.setName(rs.getString("name"));
                result.add(developer);
            }
        } catch (Exception e) {
            throw new PersistException(e);
        }
        return result;
    }

    @Override
    public Developer persist(Developer object) throws PersistException {
        super.persist(object);
        Set<Skill> skills = new HashSet<>(object.getSkills());

        for (Skill skill :
                skills) {
            try (PreparedStatement statement = connection.prepareStatement(INSERT_SKILL_DEV)) {
                statement.setLong(1, skill.getId());
                statement.setLong(2, object.getId());
                int count = statement.executeUpdate();
                if (count != 1) {
                    throw new PersistException("On persist modify more then 1 record: " + count);
                }
            } catch (Exception e) {
                throw new PersistException(e);
            }
        }

        return object;
    }

    @Override
    public Developer getByPK(Long key) throws PersistException {
        Developer developer = super.getByPK(key);
        MySqlSkillDao skillDao = new MySqlSkillDao(connection);
        Set<Skill> skills;

        try (PreparedStatement statement = connection.prepareStatement(SELECT_SKILL_DEV)) {
            statement.setLong(1, key);
            ResultSet rs = statement.executeQuery();
            skills = new HashSet<>(skillDao.parseResultSet(rs));
        } catch (Exception e) {
            throw new PersistException(e);
        }
        developer.setSkills(skills);

        return developer;
    }

    @Override
    public void delete(Developer object) throws PersistException {
        try (PreparedStatement statement = connection.prepareStatement(DELETE_SKILL_DEV)) {
            statement.setObject(1, object.getId());
            statement.executeUpdate();
        } catch (Exception e) {
            throw new PersistException(e);
        }
        super.delete(object);
    }

    @Override
    public List<Developer> getAll() throws PersistException {
        List<Developer> developers = new LinkedList<>(super.getAll());
        MySqlSkillDao skillDao = new MySqlSkillDao(connection);
        Set<Skill> skills;

        for (Developer developer :
                developers) {
            try (PreparedStatement statement = connection.prepareStatement(SELECT_SKILL_DEV)) {
                statement.setLong(1, developer.getId());
                ResultSet rs = statement.executeQuery();
                skills = new HashSet<>(skillDao.parseResultSet(rs));
            } catch (Exception e) {
                throw new PersistException(e);
            }
            developer.setSkills(skills);
        }

        return developers;
    }

    @Override
    public void prepareStatementForInsert(PreparedStatement statement, Developer object) throws PersistException {
        try {
            statement.setLong(1, object.getId());
            statement.setString(2, object.getName());
        } catch (Exception e) {
            throw new PersistException(e);
        }
    }

    @Override
    public void prepareStatementForUpdate(PreparedStatement statement, Developer object) throws PersistException {
        try {
            statement.setString(1, object.getName());
            statement.setLong(2, object.getId());
        } catch (Exception e) {
            throw new PersistException(e);
        }
    }


}
