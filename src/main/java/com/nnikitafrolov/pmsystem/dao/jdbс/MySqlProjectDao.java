package com.nnikitafrolov.pmsystem.dao.jdbс;

import com.nnikitafrolov.pmsystem.dao.PersistException;
import com.nnikitafrolov.pmsystem.model.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Class that provides a implementation of CRUD  operations for entity {@link Project},
 * that extends {@link AbstractJDBCDao}
 *
 * @author Frolov Nikita
 */
public class MySqlProjectDao extends AbstractJDBCDao<Project, Long> {

    private static final String SELECT = "SELECT id, name FROM projects";
    private static final String INSERT = "INSERT INTO projects (id, name) VALUES (?, ?);";
    private static final String UPDATE = "UPDATE projects SET name = ? WHERE id= ?;";
    private static final String DELETE = "DELETE FROM projects WHERE id= ?;";

    public MySqlProjectDao(Connection connection) {
        super(connection);
    }

    @Override
    public String getSelectQuery() {
        return SELECT;
    }

    @Override
    public String getCreateQuery() {
        return INSERT;
    }

    @Override
    public String getUpdateQuery() {
        return UPDATE;
    }

    @Override
    public String getDeleteQuery() {
        return DELETE;
    }

    @Override
    public List<Project> parseResultSet(ResultSet rs) throws PersistException {
        List<Project> result = new LinkedList<>();
        try {
            while (rs.next()) {
                Project project = new Project();
                project.setId(rs.getLong("id"));
                project.setName(rs.getString("name"));
                result.add(project);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public void prepareStatementForInsert(PreparedStatement statement, Project object) throws PersistException {
        try {
            statement.setLong(1, object.getId());
            statement.setString(2, object.getName());
        } catch (Exception e) {
            throw new PersistException(e);
        }
    }

    @Override
    public void prepareStatementForUpdate(PreparedStatement statement, Project object) throws PersistException {
        try {
            statement.setString(1, object.getName());
            statement.setLong(2, object.getId());
        } catch (Exception e) {
            throw new PersistException(e);
        }
    }
}
