package com.nnikitafrolov.pmsystem.dao.jdbс;

import com.nnikitafrolov.pmsystem.dao.PersistException;
import com.nnikitafrolov.pmsystem.model.Skill;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

/**
 * Class that provides a implementation of CRUD  operations for entity {@link Skill},
 * that extends {@link AbstractJDBCDao}
 *
 * @author Nikita Frolov
 */
public class MySqlSkillDao extends AbstractJDBCDao<Skill, Long> {

    private static final String SELECT = "SELECT id, name FROM skills";
    private static final String INSERT = "INSERT INTO skills (id, name) VALUES (?, ?);";
    private static final String UPDATE = "UPDATE skills SET name = ? WHERE id= ?;";
    private static final String DELETE = "DELETE FROM skills WHERE id= ?;";

    MySqlSkillDao(Connection connection) {
        super(connection);
    }

    @Override
    public String getSelectQuery() {
        return SELECT;
    }

    @Override
    public String getCreateQuery() {
        return INSERT;
    }

    @Override
    public String getUpdateQuery() {
        return UPDATE;
    }

    @Override
    public String getDeleteQuery() {
        return DELETE;
    }

    @Override
    public List<Skill> parseResultSet(ResultSet rs) throws PersistException {
        LinkedList<Skill> result = new LinkedList<>();
        try {
            while (rs.next()) {
                Skill skill = new Skill();
                skill.setId(rs.getLong("id"));
                skill.setName(rs.getString("name"));
                result.add(skill);
            }
        } catch (Exception e) {
            throw new PersistException(e);
        }
        return result;
    }

    @Override
    public void prepareStatementForInsert(PreparedStatement statement, Skill object) throws PersistException {
        try {
            statement.setLong(1, object.getId());
            statement.setString(2, object.getName());
        } catch (Exception e) {
            throw new PersistException(e);
        }
    }

    @Override
    public void prepareStatementForUpdate(PreparedStatement statement, Skill object) throws PersistException {
        try {
            statement.setString(1, object.getName());
            statement.setLong(2, object.getId());
        } catch (Exception e) {
            throw new PersistException(e);
        }
    }
}
