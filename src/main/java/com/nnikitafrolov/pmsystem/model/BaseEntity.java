package com.nnikitafrolov.pmsystem.model;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Base abstract class that contains property 'id'.
 * Used as a class pattern for all classes that need this property.
 *
 * @author Nikita Frolov
 */
@MappedSuperclass
public abstract class BaseEntity implements Identified<Long> {

    @Id
    @Column(name = "id")
    private Long id;

    BaseEntity() {
    }

    BaseEntity(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isNew() {
        return (this.id == null);
    }
}