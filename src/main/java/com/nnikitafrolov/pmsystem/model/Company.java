package com.nnikitafrolov.pmsystem.model;

import javax.persistence.*;
import java.util.Set;

/**
 * Class that extends {@link NameEntity} and represent entity Company,
 * adding property 'projects'.
 * Used as a based class for create object this entity.
 *
 * @author Nikita Frolov
 */
@Entity
@Table(name = "companies")
public class Company extends NameEntity {
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "projects_companies",
            joinColumns = {@JoinColumn(name = "companie_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "project_id", referencedColumnName = "id")})
    private Set<Project> projects;

    public Company() {
    }

    public Set<Project> getProjects() {
        return projects;
    }

    public void setProjects(Set<Project> projects) {
        this.projects = projects;
    }
}
