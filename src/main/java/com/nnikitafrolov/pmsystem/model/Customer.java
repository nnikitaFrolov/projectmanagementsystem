package com.nnikitafrolov.pmsystem.model;

import javax.persistence.*;
import java.util.Set;

/**
 * Class that extends {@link NameEntity} and represent entity Customer,
 * adding property 'projects'.
 * Used as a based class for create object this entity.
 *
 * @author Nikita Frolov
 */
@Entity
@Table(name = "customers")
public class Customer extends NameEntity {
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "projects_customers",
            joinColumns = {@JoinColumn(name = "customer_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "project_id", referencedColumnName = "id")})
    private Set<Project> projects;

    public Customer() {
    }

    public Set<Project> getProjects() {
        return projects;
    }

    public void setProjects(Set<Project> projects) {
        this.projects = projects;
    }
}
