package com.nnikitafrolov.pmsystem.model;

import javax.persistence.*;
import java.util.Set;

/**
 * Class that extends {@link NameEntity} and represent entity Developer,
 * adding property 'skills'.
 * Used as a based class for create object this entity.
 *
 * @author Nikita Frolov
 */

@Entity
@Table(name = "developers")
public class Developer extends NameEntity {
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "skills_developers",
            joinColumns = {@JoinColumn(name = "developer_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "skill_id", referencedColumnName = "id")})
    private Set<Skill> skills;

    @ManyToMany(mappedBy = "developers")
    private Set<Project> projects;

    public Developer() {
    }

    public Developer(Set<Skill> skills, Set<Project> projects) {
        this.skills = skills;
        this.projects = projects;
    }

    public Set<Skill> getSkills() {
        return skills;
    }

    public void setSkills(Set<Skill> skills) {
        this.skills = skills;
    }

    @Override
    public String toString() {
        return "Developer{" +
                "skills = " + skills +
                '}';
    }
}
