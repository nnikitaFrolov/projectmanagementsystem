package com.nnikitafrolov.pmsystem.model;

import java.io.Serializable;

/**
 * Interface identifiable objects.
 *
 * @param <PK> type primary key that extends {@link Serializable}
 * @author Nikita Frolov
 */
public interface Identified<PK extends Serializable> {
    PK getId();
}
