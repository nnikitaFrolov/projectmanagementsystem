package com.nnikitafrolov.pmsystem.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 *Base abstract class that extends {@link BaseEntity} adding property 'name'.
 * Used as a class pattern for all classes that need this property.
 *
 * @author Nikita Frolov
 */
@MappedSuperclass
public abstract class NameEntity extends BaseEntity {

    @Column(name = "name")
    private String name;

    NameEntity() {
    }

    NameEntity(Long id, String name) {
        super(id);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
