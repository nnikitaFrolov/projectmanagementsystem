package com.nnikitafrolov.pmsystem.model;

import javax.persistence.*;
import java.util.Set;

/**
 * Class that extends {@link NameEntity} and represent entity Project,
 * adding property 'developers'.
 * Used as a based class for create object this entity.
 *
 * @author Nikita Frolov
 */
@Entity
@Table(name = "projects")
public class Project extends NameEntity {
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "developers_projects",
            joinColumns = {@JoinColumn(name = "project_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "developer_id", referencedColumnName = "id")})
    private Set<Developer> developers;

    @ManyToMany(mappedBy = "projects")
    private Set<Company> companies;

    @ManyToMany(mappedBy = "projects")
    private Set<Customer> customers;

    public Project() {
    }

    public Set<Developer> getDevelopers() {
        return developers;
    }

    public void setDevelopers(Set<Developer> developers) {
        this.developers = developers;
    }
}
