package com.nnikitafrolov.pmsystem.model;

import org.hibernate.annotations.Proxy;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.Set;

/**
 * Class that extends {@link NameEntity} and represents entity Skill.
 * Used as a base class for create object this entity.
 *
 * @author Nikita Frolov
 */
@Entity
@Table(name = "skills")
public class Skill extends NameEntity {

    @ManyToMany(mappedBy = "skills")
    private Set<Developer> developers;

    public Skill() {
    }

    public Skill(Long id, String name) {
        super(id, name);
    }

    @Override
    public String toString() {
        return "Skill{" +
                "id = " + super.getId() +
                ", name = " + super.getName() +
                '}';
    }
}
