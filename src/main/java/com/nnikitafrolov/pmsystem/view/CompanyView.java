package com.nnikitafrolov.pmsystem.view;

import com.nnikitafrolov.pmsystem.controller.CompanyController;
import com.nnikitafrolov.pmsystem.dao.GenericDao;
import com.nnikitafrolov.pmsystem.dao.PersistException;
import com.nnikitafrolov.pmsystem.dao.jdbс.MySqlDaoFactory;
import com.nnikitafrolov.pmsystem.model.Company;
import com.nnikitafrolov.pmsystem.model.Project;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * View class that contains methods for user-console interface for entity {@link Company},
 * that extent {@link View}
 *
 * @author Nikita Frolov
 */
class CompanyView extends View<CompanyController>{
    private Set<Project> projects;

    CompanyView(GenericDao<Company, Long> companyGenericDao) throws PersistException {
        this.controller = new CompanyController(companyGenericDao);
    }

    @Override
    void view() throws IOException, PersistException, SQLException {
        projects = new HashSet<>();
        writeMessage("****  Company ****\n" +
                "1 - Add\n2 - Remove\n" +
                "3 - Update\n4 - Search by ID\n" +
                "5 - Show the company's project\n" +
                "6 - Show all company\n7 - Return to main menu\n");

        int commandNumber = readInt();

        switch (commandNumber) {
            case 1:
                writeMessage("Enter id of company:\n");
                id = readLong();
                writeMessage("Enter name of company:\n");
                name = readString();
                setHashSetProject();
                saveCompany();
                break;
            case 2:
                writeMessage("Enter id of company:\n");
                id = readLong();
                removeCompany();
                break;
            case 3:
                writeMessage("Enter id of company:\n");
                id = readLong();
                writeMessage("Enter name of company:\n");
                name = readString();
                setHashSetProject();
                updateCompany();
                break;
            case 4:
                writeMessage("Enter id of company:\n");
                id = readLong();
                getCompanyByID();
                break;
            case 5:
                writeMessage("Enter id of company:\n");
                id = readLong();
                showProjectsByCompany();
                break;
            case 6:
                showAllCompany();
                break;
            case 7:
                writeMessage("Return to main menu\n");
                return;
            default:
                break;
        }
        view();
    }

    private void showAllCompany() throws PersistException {
        List<Company> companies = controller.getAll();
        for (Company company :
                companies) {
            writeMessage(company.getId() + " - " + company.getName() + "\n");
        }
    }

    private void getCompanyByID() throws PersistException {
        Company company;
        company = controller.getById(id);

        writeMessage("Company:\n" +
                company.getId() + " - " + company.getName() + "\n");
    }

    private void updateCompany() throws PersistException {
        controller.update(getCompany());
    }

    private void removeCompany() throws PersistException {
        controller.remove(getCompany());
    }

    private void saveCompany() throws PersistException {
        controller.save(getCompany());
    }

    private Company getCompany(){
        Company company = new Company();
        company.setId(id);
        company.setName(name);
        company.setProjects(projects);
        return company;
    }

    private void setHashSetProject() throws IOException {
        while (true) {
            Project project = new Project();
            writeMessage("Enter id of project:\n");
            project.setId(readLong());
            projects.add(project);
            writeMessage("Are you want add another one project?(yes - 1/ no - 2)\n");
            if (readInt() == 2) break;
        }
    }

    private void showProjectsByCompany() throws PersistException {
        Company company;
        company = controller.getById(id);
        String projects = "";
        for (Project project :
                company.getProjects()) {
            projects += project.getName() + ", ";
        }
        projects = projects.substring(0, projects.length() - 2);
        writeMessage("Developer:\n" +
                company.getId() + " - " + company.getName() +
                "(" + projects + ")\n");
    }
}
