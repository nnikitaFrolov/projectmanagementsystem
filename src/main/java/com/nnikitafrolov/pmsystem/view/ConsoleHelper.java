package com.nnikitafrolov.pmsystem.view;

import com.nnikitafrolov.pmsystem.dao.DaoFactory;
import com.nnikitafrolov.pmsystem.dao.PersistException;
import com.nnikitafrolov.pmsystem.model.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;

/**
 * View class that contains methods for user-console interface
 *
 * @author Nikita Frolov
 */
public class ConsoleHelper<T> {

    private static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    private DaoFactory<T> factory;
    private DeveloperView developerView;
    private SkillView skillView;
    private ProjectView projectView;
    private CompanyView companyView;
    private CustomerView customerView;

    public ConsoleHelper(DaoFactory<T> factory) throws PersistException, IOException {
        this.factory = factory;
        this.developerView = new DeveloperView(factory.getDao(factory.getContext(), Developer.class));
        this.skillView = new SkillView(factory.getDao(factory.getContext(), Skill.class));
        this.projectView = new ProjectView(factory.getDao(factory.getContext(), Project.class));
        this.companyView = new CompanyView(factory.getDao(factory.getContext(), Company.class));
        this.customerView = new CustomerView(factory.getDao(factory.getContext(), Customer.class));
    }

    private static void writeMessage(String message) {
        System.out.println(message);
    }

    public void consoleHelp() throws PersistException, IOException, SQLException {
        end:
        while (true) {
            writeMessage("Choose entity which you wanted apply CRUD and put enter:\n" +
                    "1 - Developer\n2 - Skill\n3 - Project\n" +
                    "4 - Company\n5 - Customer\n6 - Exit\n");
            int readChoice = readInt();
            switch (readChoice) {
                case 1:
                    developerView.view();
                    break;
                case 2:
                    skillView.view();
                    break;
                case 3:
                    projectView.view();
                    break;
                case 4:
                    companyView.view();
                    break;
                case 5:
                    customerView.view();
                    break;
                case 6:
                    writeMessage("Exiting....");
                    System.exit(1);
                default:
                    break end;
            }
        }
        factory.closeContext();
    }

    private int readInt() throws IOException {
        int number;
        try {
            number = Integer.parseInt(bufferedReader.readLine());
        } catch (NumberFormatException e) {
            writeMessage("Entered incorrect data, please repeat enter.");
            number = readInt();
        }
        return number;
    }
}
