package com.nnikitafrolov.pmsystem.view;

import com.nnikitafrolov.pmsystem.controller.CustomerController;
import com.nnikitafrolov.pmsystem.dao.GenericDao;
import com.nnikitafrolov.pmsystem.dao.PersistException;
import com.nnikitafrolov.pmsystem.dao.jdbс.MySqlDaoFactory;
import com.nnikitafrolov.pmsystem.model.Customer;
import com.nnikitafrolov.pmsystem.model.Project;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * View class that contains methods for user-console interface for entity {@link Customer},
 * that extent {@link View}
 *
 * @author Nikita Frolov
 */
class CustomerView extends View<CustomerController> {
    private Set<Project> projects;

    CustomerView(GenericDao<Customer, Long> customerGenericDao) throws PersistException {
        this.controller = new CustomerController(customerGenericDao);
    }

    @Override
    void view() throws IOException, PersistException, SQLException {
        projects = new HashSet<>();
        writeMessage("****  Customer ****\n" +
                "1 - Add\n2 - Remove\n" +
                "3 - Update\n4 - Search by ID\n" +
                "5 - Show the customer's project\n" +
                "6 - Show all customer\n7 - Return to main menu\n");

        int commandNumber = readInt();

        switch (commandNumber) {
            case 1:
                writeMessage("Enter id of customer:\n");
                id = readLong();
                writeMessage("Enter name of customer:\n");
                name = readString();
                setHashSetProject();
                saveCustomer();
                break;
            case 2:
                writeMessage("Enter id of customer:\n");
                id = readLong();
                removeCustomer();
                break;
            case 3:
                writeMessage("Enter id of customer:\n");
                id = readLong();
                writeMessage("Enter name of customer:\n");
                name = readString();
                setHashSetProject();
                updateCustomer();
                break;
            case 4:
                writeMessage("Enter id of customer:\n");
                id = readLong();
                getCustomerByID();
                break;
            case 5:
                writeMessage("Enter id of customer:\n");
                id = readLong();
                showProjectsByCustomer();
                break;
            case 6:
                showAllCustomer();
                break;
            case 7:
                writeMessage("Return to main menu\n");
                return;
            default:
                break;
        }
        view();
    }

    private void showAllCustomer() throws PersistException {
        List<Customer> customers = controller.getAll();
        for (Customer customer :
                customers) {
            writeMessage(customer.getId() + " - " + customer.getName() + "\n");
        }
    }

    private void getCustomerByID() throws PersistException {
        Customer customer;
        customer = controller.getById(id);

        writeMessage("Customer:\n" +
                customer.getId() + " - " + customer.getName() + "\n");
    }

    private void updateCustomer() throws PersistException {
        controller.update(getCustomer());
    }

    private void removeCustomer() throws PersistException {
        controller.remove(getCustomer());
    }

    private void saveCustomer() throws PersistException {
        controller.save(getCustomer());
    }

    private Customer getCustomer() {
        Customer customer = new Customer();
        customer.setId(id);
        customer.setName(name);
        customer.setProjects(projects);
        return customer;
    }

    private void setHashSetProject() throws IOException {
        while (true) {
            Project project = new Project();
            writeMessage("Enter id of project:\n");
            project.setId(readLong());
            projects.add(project);
            writeMessage("Are you want add another one project?(yes - 1/ no - 2)\n");
            if (readInt() == 2) break;
        }
    }

    private void showProjectsByCustomer() throws PersistException {
        Customer customer;
        customer = controller.getById(id);
        String projects = "";
        for (Project project :
                customer.getProjects()) {
            projects += project.getName() + ", ";
        }
        projects = projects.substring(0, projects.length() - 2);
        writeMessage("Developer:\n" +
                customer.getId() + " - " + customer.getName() +
                "(" + projects + ")\n");
    }
}
