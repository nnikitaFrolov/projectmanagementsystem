package com.nnikitafrolov.pmsystem.view;

import com.nnikitafrolov.pmsystem.controller.DeveloperController;
import com.nnikitafrolov.pmsystem.dao.GenericDao;
import com.nnikitafrolov.pmsystem.dao.PersistException;
import com.nnikitafrolov.pmsystem.dao.jdbс.MySqlDaoFactory;
import com.nnikitafrolov.pmsystem.model.Developer;
import com.nnikitafrolov.pmsystem.model.Skill;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * View class that contains methods for user-console interface for entity {@link Developer},
 * that extent {@link View}
 *
 * @author Nikita Frolov
 */
class DeveloperView extends View<DeveloperController>{
    private Set<Skill> skills;

    DeveloperView(GenericDao<Developer, Long> developerGenericDao) throws PersistException {
        this.controller = new DeveloperController(developerGenericDao);
    }

    private void setHashSetSkills() throws IOException {
        while (true) {
            Skill skill = new Skill();
            writeMessage("Enter id of skill:\n");
            skill.setId(readLong());
            skills.add(skill);
            writeMessage("Are you want add another one skill?(yes - 1/ no - 2)\n");
            if (readInt() == 2) break;
        }
    }

    private void showSkillsByDeveloper() throws PersistException {
        Developer developer;
        developer = controller.getById(id);
        StringBuilder skills = new StringBuilder();
        for (Skill skill :
                developer.getSkills()) {
            skills.append(skill.getName()).append(", ");
        }
        skills = new StringBuilder(skills.substring(0, skills.length() - 2));
        writeMessage("Developer:\n" +
                developer.getId() + " - " + developer.getName() +
                "(" + skills + ")\n");
    }

    private void showAllDeveloper() throws PersistException {
        List<Developer> allDeveloper = controller.getAll();
        for (Developer developer :
                allDeveloper) {
            writeMessage(developer.getId() + " - " + developer.getName() + "\n");
        }
    }

    private void getDeveloperByID() throws PersistException {
        Developer developer;
        developer = controller.getById(id);

        writeMessage("Developer:\n" +
                developer.getId() + " - " + developer.getName() + "\n");
    }

    private void updateDeveloper() throws PersistException {
        controller.update(getDeveloper());
    }

    private Developer getDeveloper() {
        Developer developer = new Developer();
        developer.setId(id);
        developer.setName(name);
        developer.setSkills(skills);
        return developer;
    }

    private void removeDeveloper() throws PersistException {
        Developer developer = new Developer();
        developer.setId(id);
        developer.setSkills(skills);
        controller.remove(developer);
    }

    private void saveDeveloper() throws PersistException {
        controller.save(getDeveloper());
    }

    @Override
    void view() throws IOException, PersistException, SQLException {
        skills = new HashSet<>();
        writeMessage("****  Developer ****\n" +
                "1 - Add\n2 - Remove\n" +
                "3 - Update\n4 - Search by ID\n" +
                "5 - Show the developer's skills\n" +
                "6 - Show all developers\n7 - Return to main menu\n");

        int commandNumber = readInt();

        switch (commandNumber) {
            case 1:
                writeMessage("Enter id of developer:\n");
                id = readLong();
                writeMessage("Enter name of developer:\n");
                name = readString();
                setHashSetSkills();
                saveDeveloper();
                break;
            case 2:
                writeMessage("Enter id of developer:\n");
                id = readLong();
                removeDeveloper();
                break;
            case 3:
                writeMessage("Enter id of developer:\n");
                id = readLong();
                writeMessage("Enter name of developer:\n");
                name = readString();
                setHashSetSkills();
                updateDeveloper();
                break;
            case 4:
                writeMessage("Enter id of developer:\n");
                id = readLong();
                getDeveloperByID();
                break;
            case 5:
                writeMessage("Enter id of developer:\n");
                id = readLong();
                showSkillsByDeveloper();
                break;
            case 6:
                showAllDeveloper();
                break;
            case 7:
                writeMessage("Return to main menu\n");
                return;
            default:
                break;
        }
        view();
    }
}
