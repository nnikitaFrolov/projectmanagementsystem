package com.nnikitafrolov.pmsystem.view;

import com.nnikitafrolov.pmsystem.controller.ProjectController;
import com.nnikitafrolov.pmsystem.dao.GenericDao;
import com.nnikitafrolov.pmsystem.dao.PersistException;
import com.nnikitafrolov.pmsystem.model.Developer;
import com.nnikitafrolov.pmsystem.model.Project;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * View class that contains methods for user-console interface for entity {@link Project},
 * that extent {@link View}
 *
 * @author Nikita Frolov
 */
class ProjectView  extends View<ProjectController>{
    private Set<Developer> developers;

    @Override
    void view() throws IOException, PersistException, SQLException {
        developers = new HashSet<>();
        writeMessage("****  Project ****\n" +
                "1 - Add\n2 - Remove\n" +
                "3 - Update\n4 - Search by ID\n" +
                "5 - Show the project's developer\n" +
                "6 - Show all project\n7 - Return to main menu\n");

        int commandNumber = readInt();

        switch (commandNumber) {
            case 1:
                writeMessage("Enter id of project:\n");
                id = readLong();
                writeMessage("Enter name of project:\n");
                name = readString();
                setHashSetDeveloper();
                saveProject();
                break;
            case 2:
                writeMessage("Enter id of project:\n");
                id = readLong();
                removeProject();
                break;
            case 3:
                writeMessage("Enter id of project:\n");
                id = readLong();
                writeMessage("Enter name of project:\n");
                name = readString();
                setHashSetDeveloper();
                updateProject();
                break;
            case 4:
                writeMessage("Enter id of project:\n");
                id = readLong();
                getProjectByID();
                break;
            case 5:
                writeMessage("Enter id of project:\n");
                id = readLong();
                showDevelopersByProject();
                break;
            case 6:
                showAllProject();
                break;
            case 7:
                writeMessage("Return to main menu\n");
                return;
            default:
                break;
        }
        view();
    }

    private void showAllProject() throws PersistException {
        List<Project> projects = controller.getAll();
        for (Project project :
                projects) {
            writeMessage(project.getId() + " - " + project.getName() + "\n");
        }
    }

    private void getProjectByID() throws PersistException {
        Project project;
        project = controller.getById(id);

        writeMessage("Project:\n" +
                project.getId() + " - " + project.getName() + "\n");
    }

    private void updateProject() throws PersistException {
        controller.update(getProject());
    }

    private void removeProject() throws PersistException {
        controller.remove(getProject());
    }

    private void saveProject() throws PersistException {
        controller.save(getProject());
    }

    private Project getProject() {
        Project project = new Project();
        project.setId(id);
        project.setName(name);
        project.setDevelopers(developers);
        return project;
    }

    ProjectView(GenericDao<Project, Long> projectGenericDao) throws PersistException {
        this.controller = new ProjectController(projectGenericDao);
    }

    private void setHashSetDeveloper() throws IOException {
        while (true) {
            Developer developer = new Developer();
            writeMessage("Enter id of developer:\n");
            developer.setId(readLong());
            developers.add(developer);
            writeMessage("Are you want add another one developer?(yes - 1/ no - 2)\n");
            if (readInt() == 2) break;
        }
    }

    private void showDevelopersByProject() throws PersistException {
        Project project;
        project = controller.getById(id);
        String developers = "";
        for (Developer developer :
                project.getDevelopers()) {
            developers += developer.getName() + ", ";
        }
        developers = developers.substring(0, developers.length() - 2);
        writeMessage("Developer:\n" +
                project.getId() + " - " + project.getName() +
                "(" + developers + ")\n");
    }
}
