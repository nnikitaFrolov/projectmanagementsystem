package com.nnikitafrolov.pmsystem.view;

import com.nnikitafrolov.pmsystem.controller.SkillController;
import com.nnikitafrolov.pmsystem.dao.GenericDao;
import com.nnikitafrolov.pmsystem.dao.PersistException;
import com.nnikitafrolov.pmsystem.model.Skill;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * View class that contains methods for user-console interface for entity {@link Skill},
 * that extent {@link View}
 *
 * @author Nikita Frolov
 */
class SkillView extends View<SkillController> {

    SkillView(GenericDao<Skill, Long> skillGenericDao) throws PersistException {
        this.controller = new SkillController(skillGenericDao);
    }

    private void showAllSkills() throws PersistException {
        List<Skill> allSkills = controller.getAll();
        for (Skill skill : allSkills) {
            writeMessage(skill.getId() + " - " + skill.getName() + "\n");
        }
    }

    private void getSkillByID() throws PersistException {
        Skill skill;
        skill = controller.getById(id);
        writeMessage("Skill:\n" +
                skill.getId() + " - " + skill.getName() + "\n");
    }

    private void updateSkill() throws PersistException {
        controller.update(getSkill());
    }

    private void removeSkill() throws PersistException {
        controller.remove(getSkill());
    }

    private void saveSkill() throws PersistException {
        controller.save(getSkill());
    }

    private Skill getSkill() {
        Skill skill = new Skill();
        skill.setId(id);
        skill.setName(name);
        return skill;
    }

    @Override
    void view() throws IOException, PersistException, SQLException {
        writeMessage("****  Skill ****\n" +
                "1 - Add\n2 - Remove\n" +
                "3 - Update\n4 - Search by ID\n" +
                "5 - Show all skills\n6 - Return to main menu\n");

        int commandNumber = readInt();

        switch (commandNumber) {
            case 1:
                writeMessage("Enter id of skill:\n");
                id = readLong();
                writeMessage("Enter name of skill:\n");
                name = readString();
                saveSkill();
                break;
            case 2:
                writeMessage("Enter id of skill:\n");
                id = readLong();
                removeSkill();
                break;
            case 3:
                writeMessage("Enter id of skill:\n");
                id = readLong();
                writeMessage("Enter name of skill:\n");
                name = readString();
                updateSkill();
                break;
            case 4:
                writeMessage("Enter id of skill:\n");
                id = readLong();
                getSkillByID();
                break;
            case 5:
                showAllSkills();
                break;
            case 6:
                writeMessage("Return to main menu\n");
                return;
            default:
                break;
        }
        view();
    }
}
