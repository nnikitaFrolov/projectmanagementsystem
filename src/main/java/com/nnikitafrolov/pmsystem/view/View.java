package com.nnikitafrolov.pmsystem.view;

import com.nnikitafrolov.pmsystem.controller.AbstractController;
import com.nnikitafrolov.pmsystem.dao.PersistException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.NoSuchElementException;

/**
 * View class that contains methods for user-console interface for entities
 *
 * @author Nikita Frolov
 */
public abstract class View<T extends AbstractController> {
    T controller;
    protected String name;
    protected Long id;

    private static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    abstract void view() throws IOException, PersistException, SQLException;

    void writeMessage(String message) {
        System.out.println(message);
    }

    int readInt() throws NoSuchElementException, IOException {
        int number;
        try {
            number = Integer.parseInt(bufferedReader.readLine());
        } catch (NumberFormatException e) {
            writeMessage("Entered incorrect data, please repeat enter.");
            number = readInt();
        }
        return number;
    }

    Long readLong() throws NoSuchElementException, IOException {
        Long number;
        try {
            number = Long.parseLong(bufferedReader.readLine());
        } catch (NumberFormatException e) {
            writeMessage("Entered incorrect data, please repeat enter.\n");
            number = readLong();
        }
        return number;
    }

    String readString() throws IOException {
        return bufferedReader.readLine();
    }
}
