INSERT INTO skills (id, name) VALUES (1, 'C#');
INSERT INTO skills (id, name) VALUES (2, 'java');
INSERT INTO skills (id, name) VALUES (3, 'javaScript');
INSERT INTO skills (id, name) VALUES (4, 'UI/UX');
INSERT INTO skills (id, name) VALUES (5, 'Piton');
INSERT INTO skills (id, name) VALUES (6, 'Rubi');

INSERT INTO companies(id, name) VALUES (1, 'OLX');
INSERT INTO companies(id, name) VALUES (2, 'TFM');
INSERT INTO companies(id, name) VALUES (3, 'DMF');

INSERT INTO customers(id, name) VALUES (1, 'Jack');
INSERT INTO customers(id, name) VALUES (2, 'Bob');
INSERT INTO customers(id, name) VALUES (3, 'Steve');

INSERT INTO projects(id, name)
VALUES (1, 'LK');
INSERT INTO projects(id, name)
VALUES (2, 'DC');
INSERT INTO projects(id, name)
VALUES (3, 'ND');
INSERT INTO projects(id, name)
VALUES (4, 'MC');

INSERT INTO developers(id, name)
VALUES (1, 'Peter');
INSERT INTO developers(id, name)
VALUES (2, 'Andrei');
INSERT INTO developers(id, name)
VALUES (3, 'Konstantin');
INSERT INTO developers(id, name)
VALUES (4, 'Asya');
INSERT INTO developers(id, name)
VALUES (5, 'Peter');

INSERT INTO skills_developers(developer_id, skill_id) VALUES (1, 1);
INSERT INTO skills_developers(developer_id, skill_id) VALUES (1, 2);
INSERT INTO skills_developers(developer_id, skill_id) VALUES (2, 2);
INSERT INTO skills_developers(developer_id, skill_id) VALUES (2, 3);
INSERT INTO skills_developers(developer_id, skill_id) VALUES (3, 4);
INSERT INTO skills_developers(developer_id, skill_id) VALUES (3, 5);
INSERT INTO skills_developers(developer_id, skill_id) VALUES (4, 6);
INSERT INTO skills_developers(developer_id, skill_id) VALUES (4, 1);
INSERT INTO skills_developers(developer_id, skill_id) VALUES (5, 3);
INSERT INTO skills_developers(developer_id, skill_id) VALUES (5, 6);
INSERT INTO skills_developers(developer_id, skill_id) VALUES (5, 4);

INSERT INTO developers_projects(developer_id, project_id) VALUES (1, 1);
INSERT INTO developers_projects(developer_id, project_id) VALUES (2, 4);
INSERT INTO developers_projects(developer_id, project_id) VALUES (3, 2);
INSERT INTO developers_projects(developer_id, project_id) VALUES (4, 2);
INSERT INTO developers_projects(developer_id, project_id) VALUES (5, 3);
INSERT INTO developers_projects(developer_id, project_id) VALUES (2, 1);
INSERT INTO developers_projects(developer_id, project_id) VALUES (3, 4);

INSERT INTO projects_companies(project_id, companie_id, share) VALUES (1, 1, 2);
INSERT INTO projects_companies(project_id, companie_id, share) VALUES (1, 2, 2);
INSERT INTO projects_companies(project_id, companie_id, share) VALUES (2, 2, 1);
INSERT INTO projects_companies(project_id, companie_id, share) VALUES (3, 3, 1);
INSERT INTO projects_companies(project_id, companie_id, share) VALUES (4, 3, 1);

INSERT INTO projects_customers(project_id, customer_id) VALUES (1, 1);
INSERT INTO projects_customers(project_id, customer_id) VALUES (2, 2);
INSERT INTO projects_customers(project_id, customer_id) VALUES (3, 3);
INSERT INTO projects_customers(project_id, customer_id) VALUES (4, 3);
INSERT INTO projects_customers(project_id, customer_id) VALUES (1, 2);