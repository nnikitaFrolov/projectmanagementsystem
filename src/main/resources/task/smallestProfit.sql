SELECT companie_id, customer_id, projects.id, cost/projects_companies.share FROM projects
  INNER JOIN (projects_companies
    INNER JOIN projects_customers
      ON projects_companies.project_id = projects_customers.project_id)
    ON projects.id = projects_companies.project_id
  GROUP BY customer_id, projects.id;