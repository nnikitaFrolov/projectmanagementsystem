package com.nnikitafrolov.pmsystem.dao;

import com.nnikitafrolov.pmsystem.model.Identified;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.List;

/**
 * Test class that contains methods for CRUD  operations
 *
 * @param <T> type of object that implementation of CRUD operations
 * @param <PK> primary key
 * @param <Context> entity that describes a session with the storage system
 * @author Nikita Frolov
 */
@RunWith(Parameterized.class)
public abstract class GenericDaoTest<T extends Identified<PK>, PK extends Long, Context> {

    /**
     * Class of the test object
     */
    Class<T> daoClass;

    /**
     * An instance of a domain object that does not have a record in the storage system
     */
    private T notPersistedDto;

    /**
     * Экземпляр тестируемого дао объекта
     */
    public abstract GenericDao<T, PK> dao();

    /**
     * The instance of the object being tested
     */
    public abstract Context context();

    @Test
    public void testCreate() throws Exception {
        Identified dto = dao().persist(notPersistedDto);

        Assert.assertNotNull(dto);
        Assert.assertNotNull(dto.getId());
    }

    @Test
    public void testPersist() throws Exception {
        Assert.assertNotNull("After persist id is null.", notPersistedDto.getId());
    }

    @Test
    public void testGetByPK() throws Exception {
        PK id = dao().persist(notPersistedDto).getId();
        Identified dto = dao().getByPK(id);
        Assert.assertNotNull(dto);
    }

    @Test
    public void testDelete() throws Exception {
        T dto = dao().persist(notPersistedDto);
        Assert.assertNotNull(dto);

        List list = dao().getAll();
        Assert.assertNotNull(list);

        int oldSize = list.size();
        Assert.assertTrue(oldSize > 0);

        dao().delete(dto);

        list = dao().getAll();
        Assert.assertNotNull(list);

        int newSize = list.size();
        Assert.assertEquals(1, oldSize - newSize);
    }

    @Test
    public void testGetAll() throws Exception {
        List list = dao().getAll();
        Assert.assertNotNull(list);
        Assert.assertTrue("Domain doesn't have row",list.size() >= 0);
    }

    GenericDaoTest(Class<T> clazz, T notPersistedDto) {
        this.daoClass = clazz;
        this.notPersistedDto = notPersistedDto;
    }
}