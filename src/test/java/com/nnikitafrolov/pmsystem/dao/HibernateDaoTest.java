package com.nnikitafrolov.pmsystem.dao;

import com.nnikitafrolov.pmsystem.dao.hibernate.HibernateDAOFactory;
import com.nnikitafrolov.pmsystem.model.*;
import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Test class that extends {@link GenericDao}
 *
 * @param <T> type of object that implementation of CRUD operations using hibernate
 * @param <PK> primary key
 * @author Nikita Frolov
 */
public class HibernateDaoTest<T extends Identified<PK>, PK extends Long> extends GenericDaoTest<T, PK, SessionFactory> {

    private SessionFactory sessionFactory;

    private GenericDao<T,PK> dao;

    private DaoFactory<SessionFactory>  factory;

    public HibernateDaoTest(Class<T> clazz, T notPersistedDto) {
        super(clazz, notPersistedDto);
        factory = new HibernateDAOFactory();
    }

    @Override
    public GenericDao<T, PK> dao() {
        return dao;
    }

    @Override
    public SessionFactory context() {
        return sessionFactory;
    }

    @Parameterized.Parameters
    public static Collection getParameters() {
        Skill skill = new Skill();
        skill.setId(1L);
        skill.setName("java");

        Skill devSkill = new Skill();
        devSkill.setId(2L);
        devSkill.setName("java");
        Set<Skill> skills = new HashSet<>();
        skills.add(devSkill);
        Developer developer = new Developer();
        developer.setId(1L);
        developer.setName("Joker");
        developer.setSkills(skills);

        Project project = new Project();
        project.setId(1L);
        project.setName("Cool");
        Set<Developer> developers = new HashSet<>();
        developers.add(developer);
        project.setDevelopers(developers);

        Company company = new Company();
        company.setId(1L);
        company.setName("SoftServe");
        Set<Project> projects = new HashSet<>();
        projects.add(project);
        company.setProjects(projects);

        Customer customer = new Customer();
        customer.setId(1L);
        customer.setName("Bob");
        customer.setProjects(projects);

        return Arrays.asList(new Object[][]{
                {Developer.class, developer},
                {Skill.class, skill},
                {Project.class, project},
                {Company.class, company},
                {Customer.class, customer}
        });
    }

    @Before
    public void setUp() throws PersistException {
        sessionFactory = factory.getContext();
        dao = factory.getDao(sessionFactory, daoClass);
    }

    @After
    public void tearDown() {
        context().close();
    }
}
