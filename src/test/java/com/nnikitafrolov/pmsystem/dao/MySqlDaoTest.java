package com.nnikitafrolov.pmsystem.dao;

import com.nnikitafrolov.pmsystem.dao.jdbс.MySqlDaoFactory;
import com.nnikitafrolov.pmsystem.model.*;
import org.junit.After;
import org.junit.Before;
import org.junit.runners.Parameterized;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Test class that extends {@link GenericDao}
 *
 * @param <T> type of object that implementation of CRUD operations
 * @param <PK> primary key
 * @author Nikita Frolov
 */
public class MySqlDaoTest<T extends Identified<PK>, PK extends Long> extends GenericDaoTest<T, PK, Connection>{

    private Connection connection;

    private GenericDao<T,PK> dao;

    private DaoFactory<Connection>  factory;

    @Parameterized.Parameters
    public static Collection getParameters() {
        Skill skill = new Skill();
        skill.setId(1L);
        skill.setName("java");

        Skill devSkill = new Skill();
        devSkill.setId(2L);
        devSkill.setName("java");
        Set<Skill>skills = new HashSet<>();
        skills.add(devSkill);
        Developer developer = new Developer();
        developer.setId(1L);
        developer.setName("Joker");
        developer.setSkills(skills);

        Project project = new Project();
        project.setId(1L);
        project.setName("Cool");

        Company company = new Company();
        company.setId(1L);
        company.setName("SoftServe");

        Customer customer = new Customer();
        customer.setId(1L);
        customer.setName("Bob");

        return Arrays.asList(new Object[][]{
                {Developer.class, developer},
                {Skill.class, skill},
                {Project.class, project},
                {Company.class, company},
                {Customer.class, customer}
        });
    }

    @Before
    public void setUp() throws PersistException, SQLException {
        connection = factory.getContext();
        connection.setAutoCommit(false);
        dao = factory.getDao(connection, daoClass);
    }

    @After
    public void tearDown() throws SQLException {
        context().rollback();
        context().close();
    }

    @Override
    public GenericDao<T, PK> dao() {
        return dao;
    }

    @Override
    public Connection context() {
        return connection;
    }

    public MySqlDaoTest(Class<T> clazz, T notPersistedDto) throws IOException {
        super(clazz, notPersistedDto);
        factory = new MySqlDaoFactory();
    }
}
